package running

import (
	"context"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"net/http"
	"time"
)

// Server creates http.Server and makes it to ListenAndServe in a separate routine
// until provided context is cancelled.
// If Server stops with error - it will be restarted.
// Server method is blocking, returns after Server's Shutdown() returns.
func Server(
	ctx context.Context,
	log *logan.Entry,
	config ServerConfig,
	handler http.Handler,
) {
	var server *http.Server

	// Starting server asynchronously
	go UntilSuccess(ctx, log, "listening_server", func(ctx context.Context) (bool, error) {
		server = &http.Server{
			Addr:         config.Address,
			Handler:      handler,
			WriteTimeout: config.RequestWriteTimeout,
		}

		log.WithField("server", config).Infof("Starting Server (listening on %s).", config.Address)
		err := server.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			return false, errors.Wrap(err, "failed to ListenAndServe (Server stopped with error), will restart")
		}

		if IsCancelled(ctx) {
			// To avoid 'unsuccessful' log on clean server stopping (via cancelled context).
			// The only possible exit out of retries.
			return true, nil
		}

		// Something really strange - Server stopped with `http.ErrServerClosed`,
		// however ctx is not cancelled. Should actually never happen, but just in case.
		return false, nil
	}, time.Second, time.Hour, nil)

	<-ctx.Done()
	// Context was cancelled, start server stopping procedure.

	if config.ShutdownPause != 0 {
		log.Infof("Will initiate stopping the server after the shutdown pause (%s).", config.ShutdownPause.String())
		time.Sleep(config.ShutdownPause)
	}

	// There is no any WaitGroups, because we wait for the Server.Shutdown() to return.
	if config.ShutdownTimeout != 0 {
		// Force shut down the server after config.ShutdownTimeout.
		log.Infof("Waiting for the server to stop, will force shutdown in %s.", config.ShutdownTimeout)
		shutdownCtx, _ := context.WithTimeout(context.Background(), config.ShutdownTimeout)
		_ = server.Shutdown(shutdownCtx)
	} else {
		// Don't shutdown the server until all current requests are finished.
		log.Info("Waiting for the server to stop (will process all open connections first).")
		_ = server.Shutdown(context.Background())
	}

	log.Info("Server stopped.")
}
