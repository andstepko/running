module gitlab.com/andstepko/running

go 1.12

replace gitlab.com/distributed_lab/logan/v3 => gitlab.com/andstepko/logan/v3 v3.7.4

require gitlab.com/distributed_lab/logan/v3 v3.0.0-00010101000000-000000000000
