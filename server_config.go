package running

import "time"

// TODO Add optional bearer token into config and check Authorization for all requests
type ServerConfig struct {
	Address             string
	RequestWriteTimeout time.Duration

	ShutdownPause   time.Duration // optional
	ShutdownTimeout time.Duration // optional
}

func (s ServerConfig) GetLoganFields() map[string]interface{} {
	return map[string]interface{}{
		"address":               s.Address,
		"request_write_timeout": s.RequestWriteTimeout,
		// Optional
		"shutdown_pause":   s.ShutdownPause,
		"shutdown_timeout": s.ShutdownTimeout,
	}
}
