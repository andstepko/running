package running

import (
	"context"
	"fmt"
	"gitlab.com/distributed_lab/logan/v3/errors"
)

// RunSafely executes provided runner with handling of panic.
// If panic happens, RunSafely returns recovered error,
// wrapped with additional message, which contains runnerName.
func RunSafely(ctx context.Context, runnerName string, runner Runner) (err error) {
	defer func() {
		if rec := recover(); rec != nil {
			// Panic happened, having recover object here.
			stackedErr := errors.WithStack(errors.FromPanic(rec))
			err = errors.Wrap(stackedErr, fmt.Sprintf("runner '%s' panicked", runnerName))
		}
	}()

	return runner(ctx)
}

func runSafelyWithSuccess(ctx context.Context, runnerName string, runner SuccessRunner) (success bool, err error) {
	defer func() {
		if rec := recover(); rec != nil {
			// Panic happened, having recover object here.
			success = false
			stackedErr := errors.WithStack(errors.FromPanic(rec))
			err = errors.Wrap(stackedErr, fmt.Sprintf("runner '%s' panicked", runnerName))
		}
	}()

	return runner(ctx)
}
