package running

import "time"

type incrementalTimer struct {
	startPeriod time.Duration
	maxPeriod   time.Duration
	multiplier  uint

	currentPeriod time.Duration
	iteration     int
}

func newIncrementalTimer(startPeriod, maxPeriod time.Duration, periodMultiplier uint) *incrementalTimer {
	result := &incrementalTimer{
		startPeriod: startPeriod,
		maxPeriod:   maxPeriod,
		multiplier:  periodMultiplier,
	}

	result.refresh()
	return result
}

func (t *incrementalTimer) next() <-chan time.Time {
	result := time.After(t.currentPeriod)

	newPeriod := t.currentPeriod * time.Duration(t.multiplier)
	if newPeriod > t.maxPeriod {
		newPeriod = t.maxPeriod
	}

	t.currentPeriod = newPeriod

	t.iteration += 1

	return result
}

func (t *incrementalTimer) refresh() {
	t.currentPeriod = t.startPeriod
	t.iteration = 0
}
