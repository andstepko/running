package running

import (
	"context"
	"fmt"
	"gitlab.com/distributed_lab/logan/v3"
	"time"
)

// WithBackoff executes the runner every 'normalPeriod',
// if the runner fails (returns error) - WithBackoff executes it again after
// the 'minRetryPeriod' and multiplies the period (in 2 by default) on each following failure;
// once runner succeed again, the execution period becomes 'normalPeriod' again.
//
// Passed 'log' can be nil - in this case no logging wil happen.
//
// If you don't need to specify any optional parameters, pass nil into 'opts'.
//
// MaxTries from Options is ignored in WithBackoff.
func WithBackoff(
	ctx context.Context,
	log Logger,
	runnerName string,
	runner Runner,
	normalPeriod, minRetryPeriod, maxRetryPeriod time.Duration,
	opts *Options,
) {
	if normalPeriod == 0 {
		normalPeriod = time.Second
	}

	fields := logan.F{
		"runner_name":   runnerName,
		"normal_period": normalPeriod.String(),
	}

	normalTicker := time.NewTicker(normalPeriod)
	defer func() {
		normalTicker.Stop()
	}()

	incTimer := newIncrementalTimer(minRetryPeriod, maxRetryPeriod, opts.getIncPeriodMult())

	for {
		if IsCancelled(ctx) {
			logCtxCancelledStoppingRunner(log, fields, runnerName)
			return
		}

		err := RunSafely(ctx, runnerName, runner)
		for err != nil {
			// Abnormal execution.
			logRunnerReturnedError(log, fields.Merge(logan.F{
				"retry_number": incTimer.iteration,
				"next_retry_after": incTimer.currentPeriod,
			}), err, runnerName)

			waitForCtxOrTime(ctx, incTimer.next())
			if IsCancelled(ctx) {
				logCtxCancelledStoppingRunner(log, fields, runnerName)
				return
			}

			err := RunSafely(ctx, runnerName, runner)
			if err == nil {
				if log != nil {
					log.Log(uint32(logan.InfoLevel), fields, nil, false,
						fmt.Sprintf("Runner '%s' succeeded, returning to normal execution.", runnerName))
				}
				incTimer.refresh()
			}
		}

		waitForCtxOrTime(ctx, normalTicker.C)
	}
}

func waitForCtxOrTime(ctx context.Context, timeChan <-chan time.Time) {
	select {
	case <-ctx.Done():
		return
	case <-timeChan:
		return
	}
}
