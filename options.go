package running

const (
	defaultIncPeriodMult = 2
)

// Options is the struct to pass optional running parameters into running functions.
type Options struct {
	// MaxTries defines maximum number of tries (by default 0 - tries are not limited).
	MaxTries uint64
	// IncrementalPeriodMultiplier defines which multiplier to use
	// on each iteration of retries in the incremental timer (default is 2).
	IncrementalPeriodMultiplier uint
}

func (o *Options) getIncPeriodMult() uint {
	if o == nil {
		return defaultIncPeriodMult
	}

	return o.IncrementalPeriodMultiplier
}

func (o *Options) getMaxTries() uint64 {
	if o == nil {
		return 0
	}

	return o.MaxTries
}
