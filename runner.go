package running

import "context"

// SuccessRunner describes how success-runner function must look like.
type SuccessRunner func(context.Context) (bool, error)

// Runner describes how runner function must look like.
type Runner func(context.Context) error

// CreateSuccessRunner builds a SuccessRunner function out of the Runner function.
func CreateSuccessRunner(r Runner) SuccessRunner {
	return func(ctx context.Context) (bool, error) {
		err := r(ctx)
		if err != nil {
			return false, err
		} else {
			return true, nil
		}
	}
}
