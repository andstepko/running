package running

import (
	"fmt"
	"gitlab.com/distributed_lab/logan/v3"
)

// Logger is to avoid dependency on any particular logger
// (logrus and logan loggers implement this interface).
type Logger interface {
	Log(level uint32, fields map[string]interface{}, err error, withStack bool, args ...interface{})
}

func logCtxCancelledStoppingRunner(log Logger, fields map[string]interface{}, runnerName string) {
	if log == nil {
		return
	}

	log.Log(uint32(logan.InfoLevel), fields, nil, false,
		fmt.Sprintf("Context is canceled - stopping '%s' runner.", runnerName))
}

func logRunnerReturnedError(log Logger, fields map[string]interface{}, err error, runnerName string) {
	if log == nil {
		return
	}

	log.Log(uint32(logan.ErrorLevel), fields, err, true,
		fmt.Sprintf("Runner '%s' returned error.", runnerName))
}

func logRunOutOfTries(log Logger, runnerName string, maxTries uint64) {
	if log == nil {
		return
	}

	log.Log(uint32(logan.InfoLevel), logan.F{"runner_name": runnerName}, nil, true,
		fmt.Sprintf("Runner '%s' ran out of tries (%d), stopping the runner without reaching success.",
			runnerName, maxTries))
}
