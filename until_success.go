package running

import (
	"context"
	"time"

	"fmt"

	"gitlab.com/distributed_lab/logan/v3"
)

// UntilSuccess executes the runner until a) it returns true,nil or
// b) provided ctx is cancelled or c) MaxTries from Options (if provided) ran out.
//
// Passed 'log' can be nil - in this case no logging wil happen.
func UntilSuccess(
	ctx context.Context,
	log Logger,
	runnerName string,
	runner SuccessRunner,
	minRetryPeriod, maxRetryPeriod time.Duration,
	opts *Options,
) {
	success, err := runSafelyWithSuccess(ctx, runnerName, runner)
	if success && err == nil {
		// Instant success, hooray!
		return
	}

	tries := uint64(1)
	incrementalTimer := newIncrementalTimer(minRetryPeriod, maxRetryPeriod, opts.getIncPeriodMult())

	fields := logan.F{
		"runner":           runnerName,
	}

	for !success || err != nil {
		if opts.getMaxTries() != 0 && tries >= opts.getMaxTries() {
			logRunOutOfTries(log, runnerName, opts.getMaxTries())
			return
		}

		fields["retry_iteration"] = incrementalTimer.iteration
		fields["next_retry_after"] = incrementalTimer.currentPeriod

		// Log
		if log != nil {
			if err != nil {
				logRunnerReturnedError(log, fields, err, runnerName)
			} else {
				// Just a failure (not success), but without an error.
				log.Log(uint32(logan.InfoLevel), fields, nil, false,
					fmt.Sprintf("Runner '%s' didn't meet success, will try again.", runnerName))
			}
		}

		select {
		case <-ctx.Done():
			return
		case <-incrementalTimer.next():
			// Additional mean to guarantee *no* work is being done (no running happen) after ctx was cancelled.
			if IsCancelled(ctx) {
				return
			}

			// Trying once more.
			success, err = runSafelyWithSuccess(ctx, runnerName, runner)
		}
	}
}
